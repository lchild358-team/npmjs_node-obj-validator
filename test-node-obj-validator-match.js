module.exports = function(){

  const V = require('./node-obj-validator.js');

  return {
    data_constructor_N000: ()=>{
      return [
        'abc',
        /abc/
      ];
    },
    test_constructor_N000: (assert, data)=>{
      assert.assertNotError(()=>{
        const v = new V({
          match: data
        });
      });
    },
    data_constructor_N001: ()=>{
      return [
        'abc',
        /abc/
      ];
    },
    test_constructor_N001: (assert, data)=>{
      assert.assertNotError(()=>{
        const v = new V({
          match: {
            pattern: data
          }
        });
      });
    },
    data_constructor_E000: ()=>{
      return [
        undefined,
        null,
        true,
        false,
        0,
        0.1,
        '',
        [],
        {},
        function(){}
      ];
    },
    test_constructor_E000: (assert, data)=>{
      assert.assertError(()=>{
        const v = new V({
          match: data
        });
      });
    },
    data_constructor_E001: ()=>{
      return [
        undefined,
        null,
        true,
        false,
        0,
        0.1,
        '',
        [],
        {},
        function(){}
      ];
    },
    test_constructor_E001: (assert, data)=>{
      assert.assertError(()=>{
        const v = new V({
          match: {
            pattern: data
          }
        });
      });
    },
    data_validate_N000: ()=>{
      return [
        '^\\d+$',
        /^\d+$/
      ];
    },
    test_validate_N000: (assert, data)=>{
      const v = new V({
        match: data
      });

      assert
      .assertFalse(v.validate(''))
      .assertTrue(v.validate('0'))
      .assertTrue(v.validate(0))
      .assertTrue(v.validate('1'))
      .assertTrue(v.validate(1))
      .assertTrue(v.validate('123'))
      .assertTrue(v.validate(123))
      .assertFalse(v.validate('a'))
      .assertFalse(v.validate('abc'))
      .assertFalse(v.validate(0.1))
      .assertTrue(v.validate(1e2))
      .assertTrue(v.validate(0xff));
    },
    data_validate_N001: ()=>{
      return [
        '^\\d+$',
        /^\d+$/
      ];
    },
    test_validate_N001: (assert, data)=>{
      const v = new V({
        match: {
          pattern: data
        }
      });

      assert
      .assertFalse(v.validate(''))
      .assertTrue(v.validate('0'))
      .assertTrue(v.validate(0))
      .assertTrue(v.validate('1'))
      .assertTrue(v.validate(1))
      .assertTrue(v.validate('123'))
      .assertTrue(v.validate(123))
      .assertFalse(v.validate('a'))
      .assertFalse(v.validate('abc'))
      .assertFalse(v.validate(0.1))
      .assertTrue(v.validate(1e2))
      .assertTrue(v.validate(0xff));
    },
    data_onerror_N000: ()=>{
      return [
        '^\\d+$',
        /^\d+$/
      ];
    },
    test_onerror_N000: (assert, data)=>{
      const v = new V({
        match: data
      });

      v.validate('abc', (msgs)=>{
        assert
        .assertTypeof('object', msgs)
        .assertNotNull(msgs)
        .assertInstanceof(Array, msgs)
        .assertClassName('Array', msgs)
        .assertEquals(1, msgs.length)
        .assertUndefined(msgs[0].id)
        .assertEquals('abc', msgs[0].value)
        .assertEquals('Validation failed: Invalid value, must match /^\\d+$/', msgs[0].message);
      }, ()=>{
        assert.fail('onOk must not be called');
      });
    },
    test_onok_N000: (assert)=>{
      const v = new V({
        match: /^\d+$/
      });

      let rs = false;
      v.validate(123, (msgs)=>{
        assert.fail('onError must not be called');
      }, ()=>{
        rs = true;
      });
      assert.assertTrue(rs);
    },
    test_on_error_N000: (assert)=>{
      let rs = false;
      const v = new V({
        match: {
          pattern: /^\d+$/,
          on_error: (id, value, msg)=>{
            assert
            .assertUndefined(id)
            .assertEquals('abc', value)
            .assertEquals('Validation failed: Invalid value, must match /^\\d+$/', msg);

            rs = true;
          },
          on_ok: (id, value)=>{
            assert.fail('on_ok must not be called');
          }
        }
      });

      v.validate('abc');
      assert.assertTrue(rs);
    },
    test_on_ok_N000: (assert)=>{
      let rs = false;
      const v = new V({
        match: {
          pattern: /^\d+$/,
          on_error: (id, value, msg)=>{
            assert.fail('on_error must not be called');
          },
          on_ok: (id, value)=>{
            assert
            .assertUndefined(id)
            .assertEquals(123, value);

            rs = true;
          }
        }
      });
      
      v.validate(123);
      assert.assertTrue(rs);
    },
    test_messages_N000: (assert)=>{
      const v = new V({
        match: {
          pattern: /^\d+$/,
          message: 'Pattern {pattern} not matched'
        }
      });

      let rs = false;
      v.validate('abc', (msgs)=>{
        assert.assertEquals('Pattern /^\\d+$/ not matched', msgs[0].message);
        rs = true;
      });
      assert.assertTrue(rs);
    },
    test_message_N000: (assert)=>{
      let rs = false;
      const v = new V({
        match: {
          pattern: /^\d+$/,
          message: 'Pattern {pattern} not matched',
          on_error: (id, value, msg)=>{
            assert.assertEquals('Pattern /^\\d+$/ not matched', msg);
            rs = true;
          }
        }
      });

      v.validate('abc');
      assert.assertTrue(rs);
    },
  }
};

