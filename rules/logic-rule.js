module.exports = (function(){

  const util = require(`${__dirname}/../validate-util.js`);
  
  const CommonRule = require(`${__dirname}/common-rule.js`);
  
  /**
   * Required rule
   */
  const Cls = function(){
    CommonRule.call(this);

    this.logic = undefined;
    this.context = undefined;
    this.message = 'Validation failed: Logic failed';
  };

  /** Inheritance */
  Cls.prototype = Object.create(CommonRule.prototype);
  Cls.prototype.contructor = Cls;

  /**
   * @param options
   */
  Cls.parse = function(options){
    var ins = new Cls();

    ins.init(options);

    if(typeof options === 'function'){
      // Logic
      ins.logic = options;
    }else if(util.isNotNullObject(options)){
      // Logic
      if(typeof options.logic === 'function'){
        ins.logic = options.logic;
      }
      // Context
      if(util.isNotNullObject(options.context)){
        ins.context = options.context;
      }
      // Message
      if(util.isNotEmptyString(options.message)){
        ins.message = options.message;
      }
    }

    if(ins.logic === undefined){
      throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`);
    }

    return ins;
  };

  /**
   *
   */
  Cls.prototype.getMessage = function(){
    return this.message;
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  Cls.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    let rs = this.logic.call(this.context, obj);
    if(typeof(rs) === 'boolean' && rs === true){
      return this.onOk();
    }

    return this.onError();
  };

  return Cls;

})();

