module.exports = (function(){
  
  const util = require(`${__dirname}/../validate-util.js`);

  const CommonRule = require(`${__dirname}/common-rule.js`);

  const format = require('string-format');
  
  /**
   * ValueRule class
   */
  const Cls = function(){
    this.pattern    = undefined;
    
    this.message    = 'Validation failed: Invalid value, must match {pattern}';
  }
  
  /** Prototype */
  Cls.prototype = Object.create(CommonRule.prototype);
  Cls.prototype.constructor = Cls;

  /**
   * @param options
   */
  Cls.parse = function(options){
    var ins = new Cls();

    ins.init(options);

    if(util.isNotEmptyString(options)){
      // Pattern
      ins.pattern = new RegExp(options);
    }else if(util.isNotNullObject(options)){
      if(options instanceof RegExp){
        // Pattern
        ins.pattern = options;
      }else{
        // Pattern
        if(util.isNotEmptyString(options.pattern)){
          ins.pattern = new RegExp(options.pattern);
        }else if(util.isNotNullObject(options.pattern) && (options.pattern instanceof RegExp)){
          ins.pattern = options.pattern;
        }
        // Message
        if(util.isNotEmptyString(options.message)){
          ins.message = options.message;
        }
      }
    }

    if(ins.pattern === undefined){
      throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`); 
    }

    return ins;
  };

  /**
   *
   */
  Cls.prototype.getMessage = function(){
      return format(this.message, {pattern: this.pattern.toString()});
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  Cls.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    if(typeof(obj) === 'string'){
      if(this.pattern.test(obj)){
        return this.onOk();
      }else{
        return this.onError();
      }
    }else if(typeof(obj) === 'number'){
      if(this.pattern.test(obj.toString())){
        return this.onOk();
      }else{
        return this.onError();
      }
    }else{
      return this.onOk();
    }
  };

  return Cls;

})();

