module.exports = (function(){

  /**
   * CommonRule class
   */
  const Cls = function(){ };

  Cls.prototype.contructor = Cls;

  /**
   * @param f Function
   */
  Cls.prototype.addOnError = function(f){
    if(typeof f === 'function'){
      if(! this.onerrs){
        this.onerrs = [f];
      }else{
        this.onerrs[this.onerrs.length] = f;
      }
    }
  };

  /**
   * @param f Function
   */
  Cls.prototype.addOnOk = function(f){
    if(typeof f === 'function'){
      if(! this.onoks){
        this.onoks = [f];
      }else{
        this.onoks[this.onoks.length] = f;
      }
    }
  };

  /**
   *
   */
  Cls.prototype.getOnErrors = function(){
    if(this.onerrs){
      return this.onerrs;
    }else{
      return [];
    }
  };

  /**
   *
   */
  Cls.prototype.getOnOks = function(){
    if(this.onoks){
      return this.onoks;
    }else{
      return [];
    }
  };

  /**
   *
   */
  Cls.prototype.onError = function(){
    var message = this.getMessage();

    var validateContext = this.getValidateContext();

    var validateId = this.getValidateId();

    var validateObj = this.getValidateObj();

    var fs = this.getOnErrors();
    for(var f of fs){
      f(validateId, validateObj, message);
    }

    if(typeof this.validateOnError === 'function'){
      this.validateOnError.call(validateContext, validateId, validateObj, message);
    }

    return false;
  };

  /**
   *
   */
  Cls.prototype.onOk = function(){
    var validateContext = this.getValidateContext();

    var validateId = this.getValidateId();

    var validateObj = this.getValidateObj();

    var fs = this.getOnOks();
    for(var f of fs){
      f(validateId, validateObj);
    }

    if(typeof this.validateOnOk === 'function'){
      this.validateOnOk.call(validateContext, validateId, validateObj);
    }

    return true;
  };

  /**
   * @param options
   */
  Cls.prototype.init = function(options){
    if(typeof(options) === 'object' && options !== null){
      if(typeof options.on_error === 'function'){
        this.addOnError(options.on_error);
      }
      if(typeof options.on_ok === 'function'){
        this.addOnOk(options.on_ok);
      }
    }
  };

  /**
   *
   */
  Cls.prototype.getMessage = function(){
    // TODO Override
    return undefined;
  };

  Cls.prototype.getValidateContext = function(){
    // TODO Override
    return this.validateContext;
  };

  Cls.prototype.getValidateId = function(){
    // TODO Override
    return this.validateId;
  };

  Cls.prototype.getValidateObj = function(){
    // TODO Override
    return this.validateObj;
  };

  /**
   * @param id
   * @param obj
   * @param onError
   * @param onOk
   * @param context
   */
  Cls.prototype.initValidation = function(id, obj, onError, onOk, context){
    this.validateId      = id;
    this.validateObj     = obj;
    this.validateOnError = onError;
    this.validateOnOk    = onOk;
    this.validateContext = context;
  };

  Cls.valToStr = function(val){
    if(typeof val === 'undefined'){
      return 'UNDEFINED';
    }else if(typeof val === 'string'){
      if(val === ''){
        return 'EMPTY STRING';
      }else{
        return `"${val}"`;
      }
    }else if(typeof val === 'object'){
      if(val === null){
        return 'NULL';
      }else if(val instanceof Array){
        if(val.length <= 0){
          return 'EMPTY ARRAY';
        }else{
          return JSON.stringify(val);
        }
      }else{
        return JSON.stringify(val);
      }
    }else if(typeof val === 'boolean'){
      return val ? 'TRUE' : 'FALSE';
    }else if(typeof val === 'function'){
      return 'FUNCTION';
    }else{
      return val;
    }
  };

  return Cls;

})();

