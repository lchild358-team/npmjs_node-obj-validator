module.exports = (function(){
  
  const util = require(`${__dirname}/../validate-util.js`);

  const CommonRule = require(`${__dirname}/common-rule.js`);

  const format = require('string-format');
  
  /**
   * ValueRule class
   */
  const Cls = function(){
    this.rulechains = {};

    this.message        = undefined;
    this.subvalidateid  = undefined;
    this.subvalidateobj = undefined;
  }
  
  /** Prototype */
  Cls.prototype = Object.create(CommonRule.prototype);
  Cls.prototype.constructor = Cls;

  /**
   * @param options
   */
  Cls.parse = function(options){
    const RuleChain = require(`${__dirname}/../rule-chain.js`);

    var ins = new Cls();

    // ins.init(options);

    if(util.isNotNullObject(options)){
      for(var name in options){
        ins.rulechains[name] = RuleChain.parse(options[name]);
      };
    }else{
      throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`); 
    }

    return ins;
  };

  Cls.prototype.getMessage = function(){
    return this.message;
  };

  Cls.prototype.getValidateId = function(){
    return this.subvalidateid;
  };

  Cls.prototype.getValidateObj = function(){
    return this.subvalidateobj;
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  Cls.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    if(util.isNotNullObject(obj)){
      let errflg = false;

      for(var name in this.rulechains){
        let rulechain = this.rulechains[name];
        let subobj    = obj[name];

        let subid = id ? `${id}.${name}` : name;
        if(! rulechain.validate(subobj, subid)){
          errflg = true;

          let errors = rulechain.getErrors();
          for(var error of errors){

            this.message        = error.message;
            this.subvalidateid  = error.id;
            this.subvalidateobj = error.value;

            this.onError();
          }
        }
      }

      this.message        = undefined;
      this.subvalidateid  = undefined;
      this.subvalidateobj = undefined;

      return errflg ? false : this.onOk();
    }else{
      this.message        = undefined;
      this.subvalidateid  = undefined;
      this.subvalidateobj = undefined;

      return this.onOk();
    }

  };

  return Cls;

})();

