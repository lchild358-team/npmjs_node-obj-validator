module.exports = (function(){

  const util = require(`${__dirname}/../validate-util.js`);
  
  const CommonRule = require(`${__dirname}/common-rule.js`);
  
  /**
   * Required rule
   */
  const Cls = function(){
    CommonRule.call(this);

    this.email = undefined;
    this.message = 'Validation failed: Invalid email address';
  };

  /** Inheritance */
  Cls.prototype = Object.create(CommonRule.prototype);
  Cls.prototype.contructor = Cls;

  /**
   * @param options
   */
  Cls.parse = function(options){
    var ins = new Cls();

    ins.init(options);

    if(typeof options === 'boolean'){
      // Email
      ins.email = options;
    }else if(util.isNotEmptyString(options)){
      // Email
      ins.email   = true;
      // Message
      ins.message = options;
    }else if(util.isNotNullObject(options)){
      // Email
      ins.email = true;

      if(util.isNotEmptyString(options.message)){
        // Message
        ins.message = options.message;
      }
    }

    if(ins.email === undefined){
      throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`);
    }

    return ins;
  };

  /**
   *
   */
  Cls.prototype.getMessage = function(){
    return this.message;
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  Cls.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    if(! this.email){
      return this.onOk();
    }
    if(typeof obj !== 'string'){
      return this.onOk();
    }
    // http://emailregex.com/
    // /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/ // w3c
    let regx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(regx.test(obj)){
      return this.onOk();
    }

    return this.onError();
  };

  return Cls;

})();

