module.exports = (function(){
  
  const util = require(`${__dirname}/../validate-util.js`);

  const CommonRule = require(`${__dirname}/common-rule.js`);

  const filter = require('node-value-filter');

  const format = require('string-format');
  
  /**
   * ValueRule class
   */
  const Cls = function(){
    this.min            = undefined;
    this.max            = undefined;
    this.fix            = undefined;
    this.in             = [];
    
    this.min_message    = 'Validation failed: Invalid value, must be greater than or equals {min}';
    this.max_message    = 'Validation failed: Invalid value, must be less than or equals {max}';
    this.fix_message    = 'validation failed: Invalid value, must be {value}';
    this.in_message     = 'Validation failed: Invalid value, must be one of {values}';
    
    this.min_err_flg    = false;
    this.max_err_flg    = false;
    this.fix_err_flg    = false;
    this.in_err_flg     = false;
  }
  
  /** Prototype */
  Cls.prototype = Object.create(CommonRule.prototype);
  Cls.prototype.constructor = Cls;

  /**
   * @param options
   */
  Cls.parse = function(options){
    var ins = new Cls();

    ins.init(options);

    if(typeof options === 'string'){
      // Fix
      ins.fix = options;
    }else if(typeof options === 'number'){
      // Fix
      ins.fix = options;
    }else if(util.isNotEmptyArray(options)){
      // In
      ins.in = options;
    }else if(util.isNotNullObject(options)){
      // Min
      if(typeof options.min === 'number'){
        ins.min = options.min;
      }else if(typeof options.min === 'string'){
        ins.min = options.min;
      }
      // Max
      if(typeof options.max === 'number'){
        ins.max = options.max;
      }else if(typeof options.max === 'string'){
        ins.max = options.max;
      }
      // Value
      if(typeof options.value === 'string'){
        // Fix
        ins.fix = options.value;
      }else if(typeof options.value === 'number'){
        // Fix
        ins.fix = options.value;
      }else if(util.isNotEmptyArray(options.value)){
        // In
        ins.in = options.value;
      }
      // Message
      if(util.isNotEmptyString(options.message)){
        ins.min_message = ins.max_message = ins.fix_message = ins.in_message = options.message;
      }else if(util.isNotNullObject(options.message)){
        if(util.isNotEmptyString(options.message.min)){
          // Min
          ins.min_message = options.message.min;
        }else if(util.isNotEmptyString(options.message.max)){a
          // Max
          ins.max_message = options.message.max;
        }else if(util.isNotEmptyString(options.message.value)){
          // Fix, In
          ins.fix_message = ins.in_message = options.message.value;
        }
      }
    }

    if(ins.min === undefined && ins.max === undefined && ins.fix === undefined && ins.in.length <= 0){
      throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`); 
    }

    return ins;
  };

  /**
   *
   */
  Cls.prototype.getMessage = function(){
    if(this.min_err_flg){
      return format(this.min_message, {min: CommonRule.valToStr(this.min)});
    }else if(this.max_err_flg){
      return format(this.max_message, {max: CommonRule.valToStr(this.max)});
    }else if(this.fix_err_flg){
      return format(this.fix_message, {value: CommonRule.valToStr(this.fix)});
    }else{
      return format(this.in_message, {values: JSON.stringify(this.in)});
    }
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  Cls.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    if(typeof(obj) !== 'string' && typeof(obj) !== 'number') return this.onOk();

    // Min
    if(typeof this.min === 'string'){
      if(typeof obj === 'string'){
        if(obj < this.min){
          this.min_err_flg = true;
          return this.onError();
        }
      }else if(typeof obj === 'number'){
        if(filter(obj).string().get() < this.min){
          this.min_err_flg = true;
          return this.onError();
        }
      }
    }else if(typeof this.min === 'number'){
      if(typeof obj === 'number'){
        if(obj < this.min){
          this.min_err_flg = true;
          return this.onError();
        }
      }else if(typeof obj === 'string'){
        if(filter(obj).number().get() < this.min){
          this.min_err_flg = true;
          return this.onError();
        }
      }
    }
    // Max
    if(typeof this.max === 'string'){
      if(typeof obj === 'string'){
        if(obj > this.max){
          this.max_err_flg = true;
          return this.onError();
        }
      }else if(typeof obj === 'number'){
        if(filter(obj).string().get() > this.max){
          this.max_err_flg = true;
          return this.onError();
        }
      }
    }else if(typeof this.max === 'number'){
      if(typeof obj === 'number'){
        if(obj > this.max){
          this.max_err_flg = true;
          return this.onError();
        }
      }else if(typeof obj === 'string'){
        if(filter(obj).number().get() > this.max){
          this.max_err_flg = true;
          return this.onError();
        }
      }
    }
    // Fix
    if(typeof this.fix === 'string'){
      if(typeof obj === 'string'){
        if(obj !== this.fix){
          this.fix_err_flg = true;
          return this.onError();
        }
      }else if(typeof obj === 'number'){
        if(filter(obj).string().get() !== this.fix){
          this.fix_err_flg = true;
          return this.onError();
        }
      }
    }else if(typeof this.fix === 'number'){
      if(typeof obj === 'number'){
        if(obj !== this.fix){
          this.fix_err_flg = true;
          return this.onError();
        }
      }else if(typeof obj === 'string'){
        if(filter(obj).number().get() !== this.fix){
          this.fix_err_flg = true;
          return this.onError();
        }
      }
    }
    // In
    if(this.in.length > 0){
      for(var val of this.in){
        if(obj === val){
          return this.onOk();
        }
      }
      this.in_err_flg = true;
      return this.onError();
    }

    return this.onOk();
  };

  return Cls;

})();

