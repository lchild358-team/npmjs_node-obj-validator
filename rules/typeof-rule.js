module.exports = (function(){
  
  const util = require(`${__dirname}/../validate-util.js`);

  const CommonRule = require(`${__dirname}/common-rule.js`);

  const format = require('string-format');
  
  /**
   * TypeofRule class
   */
  const Cls = function(){
    this.types = [];
    this.message = 'Validation failed: Unexpected data type. Expected: {expected}';
  }
  
  /** Prototype */
  Cls.prototype = Object.create(CommonRule.prototype);
  Cls.prototype.constructor = Cls;

  /**
   * @param options
   */
  Cls.parse = function(options){
    var ins = new Cls();

    ins.init(options);

    const filter = (item)=>{
      return util.isNotEmptyString(item);
    };

    if(util.isNotEmptyString(options)){
      // Types (Single)
      ins.types = [options];
    }else if(util.isNotEmptyArray(options)){
      // Types (Multiple)
      ins.types = options.filter(filter);
    }else if(util.isNotNullObject(options)){
      // Types
      if(util.isNotEmptyString(options.types)){
        // Single
        ins.types = [options.types];
      }else if(util.isNotEmptyArray(options.types)){
        // Multiple
        ins.types = options.types.filter(filter);
      }
      // Message
      if(util.isNotEmptyString(options.message)){
        ins.message = options.message;
      }
    }

    if(util.isEmptyArray(ins.types)) throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`); 

    return ins;
  };

  /**
   *
   */
  Cls.prototype.getMessage = function(){
    return format(this.message, {expected: CommonRule.valToStr(this.types)});
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  Cls.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);
    
    const type = typeof(obj);

    if(this.types.indexOf(type) >= 0){
      return this.onOk();
    }

    return this.onError();
  };

  return Cls;

})();

