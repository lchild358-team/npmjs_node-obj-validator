module.exports = (function(){
  
  const util = require(`${__dirname}/../validate-util.js`);

  const CommonRule = require(`${__dirname}/common-rule.js`);

  const filter = require('node-value-filter');

  const format = require('string-format');
  
  /**
   * LengthRule class
   */
  const Cls = function(){
    this.min            = -1;
    this.max            = -1;
    this.length         = -1;
    
    this.min_message    = 'Validation failed: Invalid length, expected at least {min} characters';
    this.max_message    = 'Validation failed: Invalid length, expected at most {max} characters';
    this.length_message = 'Validation failed: Invalid length, expected {length} characters';
    
    this.min_err_flg    = false;
    this.max_err_flg    = false;
    this.length_err_flg = false;
  }
  
  /** Prototype */
  Cls.prototype = Object.create(CommonRule.prototype);
  Cls.prototype.constructor = Cls;

  /**
   * @param options
   */
  Cls.parse = function(options){
    var ins = new Cls();

    ins.init(options);

    if(util.isPositiveInteger(options)){
      // Length
      ins.length = options;
    }else if(util.isPositiveIntegerString(options)){
      // Length
      ins.length = filter(options).int().get();
    }else if(util.isNotNullObject(options) && ! (options instanceof Array)){
      // Min
      if(util.isPositiveInteger(options.min)){
        ins.min = options.min;
      }else if(util.isPositiveIntegerString(options.min)){
        ins.min = filter(options.min).int().get();
      }
      // Max
      if(util.isPositiveInteger(options.max)){
        ins.max = options.max;
      }else if(util.isPositiveIntegerString(options.max)){
        ins.max = filter(options.max).int().get();
      }
      // Length
      if(util.isPositiveInteger(options.length)){
        ins.length = options.length;
      }else if(util.isPositiveIntegerString(options.length)){
        ins.length = filter(options.length).int().get();
      }
      // Message
      if(util.isNotEmptyString(options.message) && ! (options.message instanceof Array)){
        ins.min_message = ins.max_message = ins.length_message = options.message;
      }else if(util.isNotNullObject(options.message)){
        // Min message
        if(util.isNotEmptyString(options.message.min)){
          ins.min_message = options.message.min;
        }
        // Max message
        if(util.isNotEmptyString(options.message.max)){
          ins.max_message = options.message.max;
        }
        // Length message
        if(util.isNotEmptyString(options.message.length)){
          ins.length_message = options.message.length;
        }
      }
    }

    if(ins.min < 0 && ins.max < 0 && ins.length < 0) throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`); 

    return ins;
  };

  /**
   *
   */
  Cls.prototype.getMessage = function(){
    if(this.min_err_flg){
      return format(this.min_message, {min: CommonRule.valToStr(this.min)});
    }else if(this.max_err_flg){
      return format(this.max_message, {max: CommonRule.valToStr(this.max)});
    }else{
      return format(this.length_message, {length: CommonRule.valToStr(this.length)});
    }
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  Cls.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    if(typeof(obj) !== 'string') return this.onOk();

    let len = obj.length;

    if(this.min >= 0 && len < this.min){
      this.min_err_flg = true;
      return this.onError();
    }

    if(this.max >= 0 && len > this.max){
      this.max_err_flg = true;
      return this.onError();
    }

    if(this.length >= 0 && len != this.length){
      this.length_err_flg = true;
      return this.onError();
    }

    return this.onOk();
  };

  return Cls;

})();

