module.exports = (function(){

  const util = require(`${__dirname}/../validate-util.js`);
  
  const CommonRule = require(`${__dirname}/common-rule.js`);
  
  /**
   * Required rule
   */
  const Cls = function(){
    CommonRule.call(this);

    this.required = undefined;
    this.message = 'Validation failed: parameter is required';
  };

  /** Inheritance */
  Cls.prototype = Object.create(CommonRule.prototype);
  Cls.prototype.contructor = Cls;

  /**
   * @param options
   */
  Cls.parse = function(options){
    var ins = new Cls();

    ins.init(options);

    if(typeof options === 'boolean'){
      // Required
      ins.required = options;
    }else if(util.isNotEmptyString(options)){
      // Required
      ins.required = true;
      // Message
      ins.message = options;
    }else if(util.isNotNullObject(options)){
      if(util.isNotEmptyString(options.message)){
        // Required
        ins.required = true;
        // Message
        ins.message = options.message;
      }
    }

    if(typeof ins.required === 'undefined') throw new Error(`Invalid validation options: ${JSON.stringify(options, null, 4)}`);

    return ins;
  };

  /**
   *
   */
  Cls.prototype.getMessage = function(){
    return this.message;
  };

  /**
   * @param obj
   * @param id
   * @param onError
   * @param onOk
   * @param context
   */
  Cls.prototype.validate = function(obj, id, onError, onOk, context){
    this.initValidation(id, obj, onError, onOk, context);

    if(! this.required){
      return this.onOk();
    }
    if(typeof obj === 'undefined'){
      return this.onError();
    }
    if(util.isNullObject(obj)){
      return this.onError();
    }
    if(util.isEmptyArray(obj)){
      return this.onError();
    }
    if(util.isEmptyString(obj)){
      return this.onError();
    }

    return this.onOk();
  };

  return Cls;

})();

