module.exports = function(){
  const UNDEFINED    = undefined;
  const NULL         = null;
  const EMPTY_ARRAY  = [];
  const EMPTY_STRING = '';
  const INT_ZERO     = 0;
  const FLOAT_ZERO   = 0.0;
  const FUNCTION     = ()=>{};
  const EMPTY_OBJECT = {};

  var V = require('./node-obj-validator.js');

  const assert_onok_msgs_one = (a, msgs, expectedId, expectedValue, expectedMessage)=>{
    a.assertClassName('Array', msgs, 'msgs is not Array')
    .assertEquals(1, msgs.length)
    .assertEquals(expectedId, msgs[0].id)
    .assertEquals(expectedValue, msgs[0].value)
    .assertEquals(expectedMessage, msgs[0].message);
  };

  return {
    // Test typeof rule
    // Test invalid typeof
    data_typeof_E000: ()=>{
      return [
        UNDEFINED
      , NULL
      , EMPTY_STRING
      , EMPTY_ARRAY
      , EMPTY_OBJECT
      , FUNCTION
      , INT_ZERO
      , FLOAT_ZERO
      , { something: 'stuff' }
      ];
    },
    test_typeof_E000: (a, data)=>{
      a.assertError(()=>{
        var v = new V({ typeof: data });
      });
    },
    // Test invalid typeof{ types: }
    data_typeof_E001: ()=>{
      return [
          UNDEFINED
        , NULL
        , EMPTY_STRING
        , EMPTY_ARRAY
        , EMPTY_OBJECT
        , FUNCTION
        , INT_ZERO
        , FLOAT_ZERO
        , [ UNDEFINED
          , NULL
          , EMPTY_STRING
          , EMPTY_ARRAY
          , EMPTY_OBJECT
          , FUNCTION
          , INT_ZERO
          , FLOAT_ZERO
          ]
        ];
    },
    test_typeof_E001: (a, data)=>{
      a.assertError(()=>{
        var v = new V({ typeof: { types: data } });
      });
    },
    // Test basic declaration
    data_typeof_N000: ()=>{
      return [
           'string'
        , ['string']
        , {types:  'string'}
        , {types: ['string']}
        , {types:  'string' , message: 'message'}
        , {types: ['string'], message: 'message'}
        ];
    },
    test_typeof_N000: (a, data)=>{
      a.assertNotError(()=>{
        var v = new V({ typeof: data });
      });
    },
    // Test basic validation
    data_typeof_N001: ()=>{
      return [
         'undefined'
      , ['undefined']
      , { types: 'undefined' }
      , { types: ['undefined'] }
      ];
    },
    test_typeof_N001: (a, data)=>{
      var v = new V({
        typeof: data
      });
      
      a.assertTrue(v.validate(UNDEFINED));
      a.assertFalse(v.validate(NULL));
    },
    // Test onError, onOk
    data_typeof_N002: ()=>{
      return [
         'undefined'
      , ['undefined']
      , { types: 'undefined' }
      , { types: ['undefined'] }
      ];
    },
    test_typeof_N002: (a, data)=>{
      var v = new V({
        typeof: data
      });
      
      var rs = false;
      v.validate(UNDEFINED, (msgs)=>{
        a.fail('onError must not be executed');
      }, ()=>{
        rs = true;
      });
      a.assertTrue(rs);

      v.validate(NULL, (msgs)=>{
        assert_onok_msgs_one(a, msgs, UNDEFINED, NULL, 'Validation failed: Unexpected data type. Expected: ["undefined"]');
      }, ()=>{
        a.fail('onOk must not be executed');
      });
    },
    // Test on_error, on_ok
    test_typeof_N003: (a)=>{
      var v = new V({
        typeof: {
          types: ['string'],
          on_error: (id, value, message)=>{
            a.assert('on_error must not be executed');
          },
          on_ok: (id, value)=>{
            a.assertUndefined(id);
            a.assertEquals('some string', value);
          }
        }
      });

      a.assertTrue(v.validate('some string'));
    },
    test_typeof_N004: (a)=>{
      var v = new V({
        typeof: {
          types: 'number',
          on_error: (id, value, message)=>{
            a.assertUndefined(id);
            a.assertEquals('some string', value);
            a.assertEquals('Validation failed: Unexpected data type. Expected: ["number"]', message);
          },
          on_ok: (id, value)=>{
            a.assert('on_ok must not be executed');
          }
        }
      });

      a.assertFalse(v.validate('some string'));
    },
    // Test error message
    test_typeof_N005: (a)=>{
      var v = new V({
        typeof: {
          types: 'null',
          message: 'Variable is not {expected}',
          on_error: (id, value, message)=>{
            a.assertUndefined(id);
            a.assertUndefined(value);
            a.assertEquals('Variable is not ["null"]', message);
          },
          on_ok: (id, value)=>{
            a.fail('on_ok must not be executed');
          }
        }
      });

      v.validate(UNDEFINED, (msgs)=>{
        assert_onok_msgs_one(a, msgs, UNDEFINED, UNDEFINED, 'Variable is not ["null"]');
      },()=>{
        a.fail('onOk must not be executed');
      });
    },
    // Test multiple types
    data_typeof_N006: ()=>{
      return [
          [UNDEFINED    , false]
        , [NULL         , true ]
        , [EMPTY_STRING , true ]
        , ['some string', true ]
        , [EMPTY_ARRAY  , true ]
        , [EMPTY_OBJECT , true ]
        , [FUNCTION     , false]
        , [INT_ZERO     , true ]
        , [FLOAT_ZERO   , true ]
        , [1234         , true ] 
      ];
    },
    test_typeof_N006: (a, data)=>{
      var v = new V({
        typeof: ['string', 'number', 'object']
      });

      a.assertEquals(data[1], v.validate(data[0]));
    },

  };
};

