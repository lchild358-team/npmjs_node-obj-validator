module.exports = function(){
  const UNDEFINED    = undefined;
  const NULL         = null;
  const EMPTY_ARRAY  = [];
  const EMPTY_STRING = '';
  const INT_ZERO     = 0;
  const FLOAT_ZERO   = 0.0;
  const FUNCTION     = ()=>{};
  const EMPTY_OBJECT = {};

  const V = require('./node-obj-validator.js');
  
  return {
    // Constructor
    // length
    data_length_N000: ()=>{
      return [INT_ZERO, FLOAT_ZERO, '0', 1, '1', 99, '99'];
    },
    test_length_N000: (a, data)=>{
      a.assertNotError(()=>{
        let v = new V({
          length: data
        });
      });
    },
    data_length_E000: ()=>{
      return [UNDEFINED, NULL, EMPTY_STRING, EMPTY_ARRAY, EMPTY_OBJECT,  FUNCTION, ' ', 'abc', 1.1, '1.1', [0], [1], [99], -1, {other:0}];
    },
    test_length_E000: (a, data)=>{
      a.assertError(()=>{
        let v = new V({
          length: data
        });
      });
    },
    // length - min
    data_length_N001: ()=>{
      return [INT_ZERO, FLOAT_ZERO, '0', 1, '1', 99, '99'];
    },
    test_length_N001: (a, data)=>{
      a.assertNotError(()=>{
        let v = new V({
          length: {
            min: data
          }
        });
      });
    },
    data_length_E001: ()=>{
      return [UNDEFINED, NULL, EMPTY_STRING, EMPTY_ARRAY, EMPTY_OBJECT,  FUNCTION, ' ', 'abc', 1.1, '1.1', [0], [1], [99], -1, {other:0}];
    },
    test_length_E001: (a, data)=>{
      a.assertError(()=>{
        let v = new V({
          length: {
            min: data
          }
        });
      });
    },
    // length - max
    data_length_N002: ()=>{
      return [INT_ZERO, FLOAT_ZERO, '0', 1, '1', 99, '99'];
    },
    test_length_N002: (a, data)=>{
      a.assertNotError(()=>{
        let v = new V({
          length: {
            max: data
          }
        });
      });
    },
    data_length_E002: ()=>{
      return [UNDEFINED, NULL, EMPTY_STRING, EMPTY_ARRAY, EMPTY_OBJECT,  FUNCTION, ' ', 'abc', 1.1, '1.1', [0], [1], [99], -1, {other:0}];
    },
    test_length_E002: (a, data)=>{
      a.assertError(()=>{
        let v = new V({
          length: {
            max: data
          }
        });
      });
    },
    // length -length
    data_length_N003: ()=>{
      return [INT_ZERO, FLOAT_ZERO, '0', 1, '1', 99, '99'];
    },
    test_length_N003: (a, data)=>{
      a.assertNotError(()=>{
        let v = new V({
          length: {
            length: data
          }
        });
      });
    },
    data_length_E003: ()=>{
      return [UNDEFINED, NULL, EMPTY_STRING, EMPTY_ARRAY, EMPTY_OBJECT,  FUNCTION, ' ', 'abc', 1.1, '1.1', [0], [1], [99], -1, {other:0}];
    },
    test_length_E003: (a, data)=>{
      a.assertError(()=>{
        let v = new V({
          length: {
            length: data
          }
        });
      });
    },
    // Validate
    // length
    data_length_N010: ()=>{
      return [0, 0.0, '0'];
    },
    test_length_N010: (assert, data)=>{
      let v = new V({ length: data });
      assert
      .assertTrue(v.validate(''))
      .assertFalse(v.validate('a'))
      .assertFalse(v.validate('aa'));
    },
    data_length_N011: ()=>{
      return [1, 1.0, '1'];
    },
    test_length_N011: (assert, data)=>{
      let v = new V({ length: data });
      assert
      .assertFalse(v.validate(''))
      .assertTrue(v.validate('a'))
      .assertFalse(v.validate('aa'));
    },
    // length - min
    data_length_N012: ()=>{
      return [1, 1.0, '1'];
    },
    test_length_N012: (assert, data)=>{
      let v = new V({ length: { min: data } });
      assert
      .assertTrue(v.validate('a'))
      .assertTrue(v.validate('bb'))
      .assertFalse(v.validate(''));
    },
    // length - max
    data_length_N013: ()=>{
      return [0, 0.0, '0'];
    },
    test_length_N013: (assert, data)=>{
      let v = new V({ length: { max: data } });
      assert
      .assertTrue(v.validate(''))
      .assertFalse(v.validate('a'))
      .assertFalse(v.validate('aa'));
    },
    // length - length
    data_length_N014: ()=>{
      return [0, 0.0, '0'];
    },
    test_length_N015: ()=>{
      let v = new V({ length: { length: data } });
      assert
      .assertTrue(v.validate(''))
      .assertFalse(v.validate('a'));
    },
    data_length_N015: ()=>{
      return [1, 1.0, '1'];
    },
    test_length_N015: (assert, data)=>{
      let v = new V({ length: { length: data } });
      assert
      .assertTrue(v.validate('a'))
      .assertFalse(v.validate('aa'))
      .assertFalse(v.validate(''));
    },
    // onError, onOk
    test_length_N016: (assert)=>{
      let v = new V({ length: 1 });

      let err = false;
      v.validate('', (msg)=>{
        err = true;
      }, ()=>{
        assert.fail('onOk must not be called');
      });
      assert.assertTrue(err);

      let ok = false;
      v.validate('a', (msg)=>{
        assert.fail('onError must not be called');
      }, ()=>{
        ok = true;
      });
      assert.assertTrue(ok);
    },
    test_length_N017: (assert)=>{
      let v = new V({ length: { min : 1 } });

      let err = false;
      v.validate('', (msg)=>{
        err = true;
      }, ()=>{
        assert.fail('onOk must not be called');
      });
      assert.assertTrue(err);

      let ok1 = false;
      v.validate('a', (msg)=>{
        assert.fail('onError must not be called');
      }, ()=>{
        ok1 = true;
      });
      assert.assertTrue(ok1);
      
      let ok2 = false;
      v.validate('aa', (msg)=>{
        assert.fail('onError must not be called');
      }, ()=>{
        ok2 = true;
      });
      assert.assertTrue(ok2);
    },
    test_length_N018: (assert)=>{
      let v = new V({ length: { max : 1 } });

      let err1 = false;
      v.validate('aa', (msg)=>{
        err1 = true;
      }, ()=>{
        assert.fail('onOk must not be called');
      });
      assert.assertTrue(err1);

      let err2 = false;
      v.validate('aaa', (msg)=>{
        err2 = true;
      }, ()=>{
        assert.fail('onOk must not be called');
      });
      assert.assertTrue(err2);


      let ok1 = false;
      v.validate('a', (msg)=>{
        assert.fail('onError must not be called');
      }, ()=>{
        ok1 = true;
      });
      assert.assertTrue(ok1);
      
      let ok2 = false;
      v.validate('', (msg)=>{
        assert.fail('onError must not be called');
      }, ()=>{
        ok2 = true;
      });
      assert.assertTrue(ok2);
    },
    test_length_N019: (assert)=>{
      let v = new V({ length: { length: 1 } });

      let err = false;
      v.validate('', (msg)=>{
        err = true;
      }, ()=>{
        assert.fail('onOk must not be called');
      });
      assert.assertTrue(err);

      let ok = false;
      v.validate('a', (msg)=>{
        assert.fail('onError must not be called');
      }, ()=>{
        ok = true;
      });
      assert.assertTrue(ok);
    },
    test_length_N020: (assert)=>{
      let v = new V({
        length: {
          min: 1,
          max: 2
        }
      });

      assert
      .assertFalse(v.validate(''))
      .assertTrue(v.validate('a'))
      .assertTrue(v.validate('aa'))
      .assertFalse(v.validate('aaa'));
    },
    // on_error, on_ok
    test_length_N021: (assert)=>{
      let err = false;
      
      let v = new V({
        length: {
          min : 1,
          on_ok: (id, value)=>{
            assert.fail('on_ok must not be called');
          },
          on_error: (id, value, msg)=>{
            err = true;
          }
        }
      });

      v.validate('');
      assert.assertTrue(err);
    },
    test_length_N022: (assert)=>{
      let ok = false;
      
      let v = new V({
        length: {
          min : 1,
          on_ok: (id, value)=>{
            ok = true;
          },
          on_error: (id, value, msg)=>{
            assert.fail('on_error must not be called');
          }
        }
      });

      v.validate('a');
      assert.assertTrue(ok);
    },
    test_length_N023: (assert)=>{
      let err = false;
      
      let v = new V({
        length: {
          max : 1,
          on_ok: (id, value)=>{
            assert.fail('on_ok must not be called');
          },
          on_error: (id, value, msg)=>{
            err = true;
          }
        }
      });

      v.validate('aa');
      assert.assertTrue(err);
    },
    test_length_N024: (assert)=>{
      let ok = false;
      
      let v = new V({
        length: {
          max : 1,
          on_ok: (id, value)=>{
            ok = true;
          },
          on_error: (id, value, msg)=>{
            assert.fail('on_error must not be called');
          }
        }
      });

      v.validate('a');
      assert.assertTrue(ok);
    },
    test_length_N025: (assert)=>{
      let err = false;
      
      let v = new V({
        length: {
          length : 1,
          on_ok: (id, value)=>{
            assert.fail('on_ok must not be called');
          },
          on_error: (id, value, msg)=>{
            err = true;
          }
        }
      });

      v.validate('aa');
      assert.assertTrue(err);
    },
    test_length_N026: (assert)=>{
      let ok = false;
      
      let v = new V({
        length: {
          length : 1,
          on_ok: (id, value)=>{
            ok = true;
          },
          on_error: (id, value, msg)=>{
            assert.fail('on_error must not be called');
          }
        }
      });

      v.validate('a');
      assert.assertTrue(ok);
    },
    // Message - min
    test_length_N030: (assert)=>{
      let v = new V({
        length: {
          min: 1
        }
      });

      v.validate('', (msgs)=>{
        assert
        .assertTypeof('object', msgs)
        .assertInstanceof(Array, msgs)
        .assertClassName('Array', msgs)
        .assertEquals(1, msgs.length)
        .assertUndefined(msgs[0].id)
        .assertEquals('', msgs[0].value)
        .assertEquals('Validation failed: Invalid length, expected at least 1 characters', msgs[0].message);
      });
    },
    test_length_N031: (assert)=>{
      let v = new V({
        length: {
          min: 1,
          message: 'Variable must be at least {min} character'
        }
      });

      v.validate('', (msgs)=>{
        assert
        .assertTypeof('object', msgs)
        .assertInstanceof(Array, msgs)
        .assertClassName('Array', msgs)
        .assertEquals(1, msgs.length)
        .assertUndefined(msgs[0].id)
        .assertEquals('', msgs[0].value)
        .assertEquals('Variable must be at least 1 character', msgs[0].message);
      });
    },
    test_length_N032: (assert)=>{
      let v = new V({
        length: {
          min: 1,
          message: {
            min: 'Variable must be at least {min} character'
          }
        }
      });

      v.validate('', (msgs)=>{
        assert
        .assertTypeof('object', msgs)
        .assertInstanceof(Array, msgs)
        .assertClassName('Array', msgs)
        .assertEquals(1, msgs.length)
        .assertUndefined(msgs[0].id)
        .assertEquals('', msgs[0].value)
        .assertEquals('Variable must be at least 1 character', msgs[0].message);
      });
    },
    test_length_N033: (assert)=>{
      let v = new V({
        length: {
          min: 1,
          on_error: (id, value, msg)=>{
            assert
            .assertUndefined(id)
            .assertEquals('', value)
            .assertEquals('Validation failed: Invalid length, expected at least 1 characters', msg);
          }
        }
      });

      v.validate('');
    },
    test_length_N034: (assert)=>{
      let v = new V({
        length: {
          min: 1,
          message: 'Variable must be at least {min} character',
          on_error: (id, value, msg)=>{
            assert
            .assertUndefined(id)
            .assertEquals('', value)
            .assertEquals('Variable must be at least 1 character', msg);
          }
        }
      });

      v.validate('');
    },
    test_length_N035: (assert)=>{
      let v = new V({
        length: {
          min: 1,
          message: {
            min: 'Variable must be at least {min} character',
          },
          on_error: (id, value, msg)=>{
            assert
            .assertUndefined(id)
            .assertEquals('', value)
            .assertEquals('Variable must be at least 1 character', msg);
          }
        }
      });

      v.validate('');
    },
    test_length_N036: (assert)=>{
      let v = new V({
        length: {
          min: 1,
          on_ok: (id, value)=>{
            assert
            .assertUndefined(id)
            .assertEquals('a', value);
          }
        }
      });

      v.validate('a');
    },
    // Message - max
    test_length_N040: (assert)=>{
      let v = new V({
        length: {
          max: 1
        }
      });

      v.validate('aa', (msgs)=>{
        assert
        .assertTypeof('object', msgs)
        .assertInstanceof(Array, msgs)
        .assertClassName('Array', msgs)
        .assertEquals(1, msgs.length)
        .assertUndefined(msgs[0].id)
        .assertEquals('aa', msgs[0].value)
        .assertEquals('Validation failed: Invalid length, expected at most 1 characters', msgs[0].message);
      });
    },
    test_length_N041: (assert)=>{
      let v = new V({
        length: {
          max: 1,
          message: 'Variable must be at most {max} character'
        }
      });

      v.validate('aa', (msgs)=>{
        assert
        .assertTypeof('object', msgs)
        .assertInstanceof(Array, msgs)
        .assertClassName('Array', msgs)
        .assertEquals(1, msgs.length)
        .assertUndefined(msgs[0].id)
        .assertEquals('aa', msgs[0].value)
        .assertEquals('Variable must be at most 1 character', msgs[0].message);
      });
    },
    test_length_N042: (assert)=>{
      let v = new V({
        length: {
          max: 1,
          message: {
            max: 'Variable must be at most {max} character'
          }
        }
      });

      v.validate('aa', (msgs)=>{
        assert
        .assertTypeof('object', msgs)
        .assertInstanceof(Array, msgs)
        .assertClassName('Array', msgs)
        .assertEquals(1, msgs.length)
        .assertUndefined(msgs[0].id)
        .assertEquals('aa', msgs[0].value)
        .assertEquals('Variable must be at most 1 character', msgs[0].message);
      });
    },
    test_length_N043: (assert)=>{
      let v = new V({
        length: {
          max: 1,
          on_error: (id, value, msg)=>{
            assert
            .assertUndefined(id)
            .assertEquals('aa', value)
            .assertEquals('Validation failed: Invalid length, expected at most 1 characters', msg);
          }
        }
      });

      v.validate('aa');
    },
    test_length_N044: (assert)=>{
      let v = new V({
        length: {
          max: 1,
          message: 'Variable must be at most {max} character',
          on_error: (id, value, msg)=>{
            assert
            .assertUndefined(id)
            .assertEquals('aa', value)
            .assertEquals('Variable must be at most 1 character', msg);
          }
        }
      });

      v.validate('aa');
    },
    test_length_N045: (assert)=>{
      let v = new V({
        length: {
          max: 1,
          message: {
            max: 'Variable must be at most {max} character',
          },
          on_error: (id, value, msg)=>{
            assert
            .assertUndefined(id)
            .assertEquals('aa', value)
            .assertEquals('Variable must be at most 1 character', msg);
          }
        }
      });

      v.validate('aa');
    },
    test_length_N046: (assert)=>{
      let v = new V({
        length: {
          max: 1,
          on_ok: (id, value)=>{
            assert
            .assertUndefined(id)
            .assertEquals('a', value);
          }
        }
      });

      v.validate('a');
    },
    // Message - max
    test_length_N050: (assert)=>{
      let v = new V({
        length: {
          length: 1
        }
      });

      v.validate('aa', (msgs)=>{
        assert
        .assertTypeof('object', msgs)
        .assertInstanceof(Array, msgs)
        .assertClassName('Array', msgs)
        .assertEquals(1, msgs.length)
        .assertUndefined(msgs[0].id)
        .assertEquals('aa', msgs[0].value)
        .assertEquals('Validation failed: Invalid length, expected 1 characters', msgs[0].message);
      });
    },
    test_length_N051: (assert)=>{
      let v = new V({
        length: {
          length: 1,
          message: 'Variable must be {length} character'
        }
      });

      v.validate('aa', (msgs)=>{
        assert
        .assertTypeof('object', msgs)
        .assertInstanceof(Array, msgs)
        .assertClassName('Array', msgs)
        .assertEquals(1, msgs.length)
        .assertUndefined(msgs[0].id)
        .assertEquals('aa', msgs[0].value)
        .assertEquals('Variable must be 1 character', msgs[0].message);
      });
    },
    test_length_N052: (assert)=>{
      let v = new V({
        length: {
          length: 1,
          message: {
            length: 'Variable must be {length} character'
          }
        }
      });

      v.validate('aa', (msgs)=>{
        assert
        .assertTypeof('object', msgs)
        .assertInstanceof(Array, msgs)
        .assertClassName('Array', msgs)
        .assertEquals(1, msgs.length)
        .assertUndefined(msgs[0].id)
        .assertEquals('aa', msgs[0].value)
        .assertEquals('Variable must be 1 character', msgs[0].message);
      });
    },
    test_length_N053: (assert)=>{
      let v = new V({
        length: {
          length: 1,
          on_error: (id, value, msg)=>{
            assert
            .assertUndefined(id)
            .assertEquals('aa', value)
            .assertEquals('Validation failed: Invalid length, expected 1 characters', msg);
          }
        }
      });

      v.validate('aa');
    },
    test_length_N054: (assert)=>{
      let v = new V({
        length: {
          length: 1,
          message: 'Variable must be {length} character',
          on_error: (id, value, msg)=>{
            assert
            .assertUndefined(id)
            .assertEquals('aa', value)
            .assertEquals('Variable must be 1 character', msg);
          }
        }
      });

      v.validate('aa');
    },
    test_length_N055: (assert)=>{
      let v = new V({
        length: {
          length: 1,
          message: {
            length: 'Variable must be {length} character',
          },
          on_error: (id, value, msg)=>{
            assert
            .assertUndefined(id)
            .assertEquals('aa', value)
            .assertEquals('Variable must be 1 character', msg);
          }
        }
      });

      v.validate('aa');
    },
    test_length_N056: (assert)=>{
      let v = new V({
        length: {
          length: 1,
          on_ok: (id, value)=>{
            assert
            .assertUndefined(id)
            .assertEquals('a', value);
          }
        }
      });

      v.validate('a');
    },
  }
};
