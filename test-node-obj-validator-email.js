module.exports = function(){

  var V = require('./node-obj-validator.js');

  return {
    data_constructor_N000: ()=>{
      return [
        true,
        false,
        'Message',
        [],
        ['abc'],
        {},
        {a:1},
        {message:''},
        {
          message: 'Message'
        }
      ];
    },
    test_constructor_N000: (a, data)=>{
      a.assertNotError(()=>{
        var v = new V({
          email: data
        });
      });
    },
    data_constructor_E000: ()=>{
      return [
        undefined,
        null,
        '',
        0,
        0.123,
        function(){}
      ];
    },
    test_constructor_E000: (a, data)=>{
      a.assertError(()=>{
        var v = new V({
          email: data
        });
      });
    },
    data_validate_N000: ()=>{
      return [
        'abc@abc.com',
        'abc.abc@abc.com',
        'abc.abc@abc.co',
        'abc-abc@abc.co',
        'abc-abc@a-bc.co',
        'abc+abc@abc.com'
      ];
    },
    test_validate_N000: (a, data)=>{
      v = new V({ email: true });
      a.assertTrue(v.validate(data));
    },
    data_validate_N001: ()=>{
      return [
        undefined,
        null,
        true,
        false,
        0,
        0.123,
        [],
        ['abc'],
        {},
        {a:1},
        function(){}
      ];
    },
    test_validate_N001: (a, data)=>{
      v = new V({ email: true });
      a.assertTrue(v.validate(data));
    },
    data_validate_N002: ()=>{
      return [
        '',
        'abc',
        'abc.@abc.com',
        '.abc@abc.com',
        'abc@.abc.com',
        'abc@abc',
        'abc@abc.',
        'abc@abc.com.',
        'abc@abc.c'
      ];
    },
    test_validate_N002: (a, data)=>{
      v = new V({ email: false });
      a.assertTrue(v.validate(data));
    },
    data_validate_E000: ()=>{
      return [
        '',
        'abc',
        'abc.@abc.com',
        '.abc@abc.com',
        'abc@.abc.com',
        'abc@abc',
        'abc@abc.',
        'abc@abc.com.',
        'abc@abc.c'
      ];
    },
    test_validate_E000: (a, data)=>{
      v = new V({ email: true });
      a.assertFalse(v.validate(data));
    },
    test_messagse_N000: (a, data)=>{
      v = new V({ email: true });
      let errflg = false;
      a.assertFalse(v.validate('abc', (msgs)=>{
        a.assertTypeof('object', msgs);
        a.assertClassName('Array', msgs);
        a.assertInstanceof(Array, msgs);
        a.assertEquals(1, msgs.length);
        a.assertUndefined(msgs[0].id);
        a.assertEquals('abc', msgs[0].value);
        a.assertEquals('Validation failed: Invalid email address', msgs[0].message);
        errflg = true;
      }, ()=>{
        a.fail('onOk must not be call');
      }));

      a.assertTrue(errflg);
    },
    test_messagse_N001: (a, data)=>{
      v = new V({ email: 'This is message' });
      let errflg = false;
      a.assertFalse(v.validate('abc', (msgs)=>{
        a.assertTypeof('object', msgs);
        a.assertClassName('Array', msgs);
        a.assertInstanceof(Array, msgs);
        a.assertEquals(1, msgs.length);
        a.assertUndefined(msgs[0].id);
        a.assertEquals('abc', msgs[0].value);
        a.assertEquals('This is message', msgs[0].message);
        errflg = true;
      }, ()=>{
        a.fail('onOk must not be call');
      }));

      a.assertTrue(errflg);
    },
    test_messagse_N002: (a, data)=>{
      v = new V({ email: { message: 'This is message' } });
      let errflg = false;
      a.assertFalse(v.validate('abc', (msgs)=>{
        a.assertTypeof('object', msgs);
        a.assertClassName('Array', msgs);
        a.assertInstanceof(Array, msgs);
        a.assertEquals(1, msgs.length);
        a.assertUndefined(msgs[0].id);
        a.assertEquals('abc', msgs[0].value);
        a.assertEquals('This is message', msgs[0].message);
        errflg = true;
      }, ()=>{
        a.fail('onOk must not be called');
      }));

      a.assertTrue(errflg);
    },
    test_onok_N000: (a, data)=>{
      v = new V({ email: true });
      let okflg = false;
      a.assertTrue(v.validate('abc@abc.com', (msgs)=>{
        a.fail('onError must not be called');
      }, ()=>{
        okflg = true;
      }));
      a.assertTrue(okflg);
    },
    test_onok_N001: (a, data)=>{
      let okflg = false;
      v = new V({ email: {
        on_ok: (id, value)=>{
          a.assertUndefined(id);
          a.assertEquals('abc@abc.com', value);
          okflg = true;
        }
      } });
      a.assertTrue(v.validate('abc@abc.com', (msgs)=>{
        a.fail('onError must not be called');
      }));
      a.assertTrue(okflg);
    },
    test_onerror_N000: (a, data)=>{
      let errflg = false;
      v = new V({ email: {
        message: 'This is message',
        on_error: (id, value, msg)=>{
          a.assertUndefined(id);
          a.assertEquals('abc', value);
          a.assertEquals('This is message', msg);
          errflg = true;
        }
      } });
      a.assertFalse(v.validate('abc', (msgs)=>{
      }, ()=>{
        a.fail('onOk must not be called');
      }));

      a.assertTrue(errflg);
    }
  };
};

