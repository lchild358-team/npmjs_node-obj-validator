var Suite = require('node-unit-test');
var suite = new Suite();

var rs = suite
.addFile(__dirname + '/test-node-obj-validator.js')
.addFile(__dirname + '/test-node-obj-validator-required.js')
.addFile(__dirname + '/test-node-obj-validator-typeof.js')
.addFile(__dirname + '/test-node-obj-validator-length.js')
.addFile(__dirname + '/test-node-obj-validator-value.js')
.addFile(__dirname + '/test-node-obj-validator-match.js')
.addFile(__dirname + '/test-node-obj-validator-child.js')
.addFile(__dirname + '/test-node-obj-validator-children.js')
.addFile(__dirname + '/test-node-obj-validator-email.js')
.addFile(__dirname + '/test-node-obj-validator-logic.js')
.execute();

process.exit(rs ? 0 : 1);
