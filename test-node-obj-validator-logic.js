module.exports = function(){

  var V = require('./node-obj-validator.js');

  return {
    data_constructor_N000: ()=>{
      return [
        ()=>{},
        {
          logic: ()=>{}
        },
        {
          logic: ()=>{},
          message: 'abc'
        }
      ];
    },
    test_constructor_N000: (a, data)=>{
      a.assertNotError(()=>{
        let v = new V({
          logic: data
        });
      });
    },
    data_constructor_E000: ()=>{
      return [
        undefined,
        null,
        '',
        'abc',
        0,
        0.123,
        {},
        {a:1},
        true,
        false,
        [],
        [1]
      ];  
    },
    test_constructor_E000: (a, data)=>{
      a.assertError(()=>{
        let v = new V({
          logic: data
        });
      });
    },
    test_validate_N000: (a, data)=>{
      let v = new V({
        logic: ()=>{return true;}
      });
      a.assertTrue(v.validate(undefined));
    },
    test_validate_N001: (a, data)=>{
      let v = new V({
        logic: ()=>{return false;}
      });
      a.assertFalse(v.validate(undefined));
    },
    data_validate_N002: ()=>{
      return [
        undefined,
        null,
        '',
        'abc',
        0,
        1,
        0.123,
        true,
        false,
        [],
        [1,2,3],
        {},
        {a:1},
        ()=>{}
      ];
    },
    test_validate_N002: (a, data)=>{
      let v = new V({
        logic: (o)=>{return o === data;}
      });
      a.assertTrue(v.validate(data));
    },
    data_validate_N003: ()=>{
      return [
        undefined,
        null,
        '',
        'abc',
        0,
        1,
        0.123,
        true,
        false,
        [],
        [1,2,3],
        {},
        {a:1},
        ()=>{}
      ];
    },
    test_validate_N003: (a, data)=>{
      let v = new V({
        logic: (o)=>{return o !== data;}
      });
      a.assertFalse(v.validate(data));
    },
    test_message_N000: (a, data)=>{
      let v = new V({
        logic: ()=>{return false;}
      });
      let errflg = false;
      a.assertFalse(v.validate(0, (msgs)=>{
        a.assertTypeof('object', msgs);
        a.assertClassName('Array', msgs);
        a.assertInstanceof(Array, msgs);
        a.assertEquals(1, msgs.length);
        a.assertUndefined(msgs[0].id);
        a.assertEquals(0, msgs[0].value);
        a.assertEquals('Validation failed: Logic failed', msgs[0].message);
        errflg = true;
      }, ()=>{
        a.fail('onOk must not be called');
      }));
    },
    test_message_N001: (a, data)=>{
      let v = new V({
        logic: {
          logic: ()=>{return false;},
          message: 'This is message'
        }
      });
      let errflg = false;
      a.assertFalse(v.validate(0, (msgs)=>{
        a.assertTypeof('object', msgs);
        a.assertClassName('Array', msgs);
        a.assertInstanceof(Array, msgs);
        a.assertEquals(1, msgs.length);
        a.assertUndefined(msgs[0].id);
        a.assertEquals(0, msgs[0].value);
        a.assertEquals('This is message', msgs[0].message);
        errflg = true;
      }, ()=>{
        a.fail('onOk must not be called');
      }));
    },
    test_onok_N000: (a, data)=>{
      let v = new V({
        logic: {
          logic: ()=>{return true;}
        }
      });
      let okflg = false;
      a.assertTrue(v.validate(0, (msgs)=>{
        a.fail('onError must not be called');
      }, ()=>{
        okflg = true;
      }));
      a.assertTrue(okflg);
    },
    test_onok_N001: (a, data)=>{
      let okflg = false;
      let v = new V({
        logic: {
          logic: ()=>{return true;},
          on_ok: (id, value)=>{
            a.assertUndefined(id);
            a.assertEquals(0, value);
            okflg = true;
          }
        }
      });
      a.assertTrue(v.validate(0, (msgs)=>{
        a.fail('onError must not be called');
      }));
      a.assertTrue(okflg);
    },
    test_onerror_N000: (a, data)=>{
      let errflg = false;
      let v = new V({
        logic: {
          logic: ()=>{return false;},
          message: 'This is message',
          on_error: (id, value, message)=>{
            a.assertUndefined(id);
            a.assertEquals(0, value);
            a.assertEquals('This is message', message);
            errflg = true;
          },
          on_ok: (id, value)=>{
            a.fail('on_ok must not be called');
          }
        }
      });
      a.assertFalse(v.validate(0, undefined, ()=>{
        a.fail('onOk must not be called');
      }));
      a.assertTrue(errflg);
    },
  };
};

