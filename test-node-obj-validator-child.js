module.exports = function(){

  const V = require('./node-obj-validator.js');

  return {
    data_constructor_N000: function(){
      return [
        {},
        {
          name: {
            required: true
          }
        },
        [],
        [
          {
            required: true
          }
        ]
      ];
    },
    test_constructor_N000: function(assert, data){
      assert.assertNotError(()=>{
        let v = new V({
          child: data
        });
      });
    },
    data_constructor_E000: function(){
      return [
        undefined,
        null,
        '',
        'abc',
        true,
        false,
        0,
        123,
        1.234,
        function(){}
      ];  
    },
    test_constructor_E000: function(assert, data){
      assert.assertError(()=>{
        let v = new V({
          child: data
        });
      });
    },
    data_validate_N000: function(){
      return [
        {},
        [],
        {b: 'value'},
        [0,1],
        {a: undefined},
        {a: null},
        {a: ''},
        {a: []}
      ];
    },
    test_validate_N000: function(assert, data){
      let v = new V({
        child: {
          a: {
            required: true
          }
        }
      });

      assert.assertFalse(v.validate(data));
    },
    data_validate_N001: function(){
      return [
        {a: 'value'},
        {a: 0},
        {a: {}}
      ];
    },
    test_validate_N001: function(assert, data){
      let v = new V({
        child: {
          a: {
            required: true
          }
        }
      });

      assert.assertTrue(v.validate(data));
    },
    data_validate_N002: function(){
      return [
        {},
        {a: undefined},
        {a: null},
        {a: ''},
        {a: []},
        {a: {}},
        {a: 0},
        {a: 0.0},
        {b: 'value'}
      ];
    },
    test_validate_N002: function(assert, data){
      let v = new V({
        child: {
          a: {
            required: false
          }
        }
      });

      assert.assertTrue(v.validate(data));
    },
    test_onError_N001: function(assert, data){
      let v = new V({
        child: {
          a: {
            required: true
          }
        }
      });

      let value = {};
      assert.assertFalse(v.validate(value, (msgs)=>{
        assert.assertTypeof('object', msgs);
        assert.assertClassName('Array', msgs);
        assert.assertInstanceof(Array, msgs);
        assert.assertEquals(1, msgs.length);
        assert.assertEquals('a', msgs[0].id);
        assert.assertEquals(value.a, msgs[0].value);
        assert.assertEquals('Validation failed: parameter is required', msgs[0].message);
      }, ()=>{
        assert.fail('onOk should not be called');
      }));
    },
    test_onError_N002: function(assert, data){
      let v = new V({
        child: {
          a: {
            required: true,
            child: {
              b: {
                required: true
              }
            }
          }
        }
      });

      let value = {};
      assert.assertFalse(v.validate(value, (msgs)=>{
        assert.assertTypeof('object', msgs);
        assert.assertClassName('Array', msgs);
        assert.assertInstanceof(Array, msgs);
        assert.assertEquals(1, msgs.length);
        assert.assertEquals('a', msgs[0].id);
        assert.assertEquals(value.a, msgs[0].value);
        assert.assertEquals('Validation failed: parameter is required', msgs[0].message);
      }, ()=>{
        assert.fail('onOk should not be called');
      }));
    },
    test_onError_N003: function(assert, data){
      let v = new V({
        child: {
          a: {
            required: true,
            child: {
              b: {
                required: true
              }
            }
          }
        }
      });

      let value = {a:{}};
      assert.assertFalse(v.validate(value, (msgs)=>{
        assert.assertTypeof('object', msgs);
        assert.assertClassName('Array', msgs);
        assert.assertInstanceof(Array, msgs);
        assert.assertEquals(1, msgs.length);
        assert.assertEquals('a.b', msgs[0].id);
        assert.assertEquals(value.a.b, msgs[0].value);
        assert.assertEquals('Validation failed: parameter is required', msgs[0].message);
      }, ()=>{
        assert.fail('onOk should not be called');
      }));
    },
    test_onOk_N001: function(assert, data){
      let v = new V({
        child: {
          a: {
            required: true
          }
        }
      });

      let rs = false;
      assert.assertTrue(v.validate({a:1}, (msgs)=>{
        assert.fail('onError should not be called');
      }, ()=>{
        rs = true;
      }));
      assert.assertTrue(rs);
    },
    test_onOk_N002: function(assert, data){
      let v = new V({
        child: {
          a: {
            child: {
              b: {
                required: true
              }
            }
          }
        }
      });

      let rs = false;
      assert.assertTrue(v.validate({}, (msgs)=>{
        assert.fail('onError should not be called');
      }, ()=>{
        rs = true;
      }));
      assert.assertTrue(rs);
    },
    test_onOk_N003: function(assert, data){
      let v = new V({
        child: {
          a: {
            child: {
              b: {
                required: true
              }
            }
          }
        }
      });

      let rs = false;
      assert.assertTrue(v.validate({a: {b: 'value'}}, (msgs)=>{
        assert.fail('onError should not be called');
      }, ()=>{
        rs = true;
      }));
      assert.assertTrue(rs);
    }
  }
};

