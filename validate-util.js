module.exports = (function(){

  /** Utility functions */
  const Util = {};

  /**
   * Convenient method
   * Check if the given object is null object
   * @param obj mixed
   * @return boolean
   */
  Util.isNullObject = function(obj){
    return typeof(obj) === 'object' && obj === null;
  };

  /**
   * Convenient method
   * Check if the given object is an object that is not null
   * @param obj mixed
   * @return boolean
   */
  Util.isNotNullObject = function(obj){
    return typeof(obj) === 'object' && obj !== null;
  };

  /**
   * Convenient method
   * Check if the given object is an empty string
   * @param obj mixed
   * @return boolean
   */
  Util.isEmptyString = function(obj){
    return typeof(obj) === 'string' && obj === '';
  };

  /**
   * Convenient method
   * Check if the given object is a string that is not empty
   * @param obj mixed
   * @return boolean
   */
  Util.isNotEmptyString = function(obj){
    return typeof(obj) === 'string' && obj !== '';
  };

  /**
   * Convenient method
   * Check if the given object is an empty array
   * @param obj mixed
   * @return boolean
   */
  Util.isEmptyArray = function(obj){
    return typeof(obj) === 'object' && (obj instanceof Array) && obj.length <= 0;
  };

  /**
   * Convenient method
   * Check if the given object is an array that not empty
   * @param obj mixed
   * @return boolean
   */
  Util.isNotEmptyArray = function(obj){
    return typeof(obj) === 'object' && (obj instanceof Array) && obj.length > 0;
  };

  /**
   * Convenient method
   * Check if the given object is an integer string
   * @param obj mixed
   * @return boolean
   */
  Util.isIntegerString = function(obj){
    return typeof(obj) === 'string' && /^-?\d+$/.test(obj);
  };

  /**
   * Convenient method
   * Check if the given object is an positive integer string
   * @param obj mixed
   * @return boolean
   */
  Util.isPositiveIntegerString = function(obj){
    return typeof(obj) === 'string' && /^\d+$/.test(obj);
  };

  /**
   * Convenient method
   * Check if the given object is an negaitive integer string
   * @param obj mixed
   * @return boolean
   */
  Util.isNegativeIntegerString = function(obj){
    return typeof(obj) === 'string' && /^-\d+$/.test(obj);
  };

  /**
   * Convenient method
   * Check if the given object is an float string
   * @param obj mixed
   * @return boolean
   */
  Util.isFloatString = function(obj){
    return typeof(obj) === 'string' && /^-?\d+\.\d+$/.test(obj);
  };

  /**
   * Convenient method
   * Check if the given object is an positive float string
   * @param obj mixed
   * @return boolean
   */
  Util.isPositiveFloatString = function(obj){
    return typeof(obj) === 'string' && /^\d+\.\d+$/.test(obj);
  };

  /**
   * Convenient method
   * Check if the given object is an negative float string
   * @param obj mixed
   * @return boolean
   */
  Util.isNegativeFloatString = function(obj){
    return typeof(obj) === 'string' && /^-\d+\.\d+$/.test(obj);
  };

  /**
   * Convenient method
   * Check if the given object is a positive number
   * @param obj mixed
   * @return boolean
   */
  Util.isPositiveNumber = function(obj){
    return typeof(obj) === 'number' && obj >= 0;
  };

  /**
   * Convenient method
   * Check if the given object is a negative number
   * @param obj mixed
   * @return boolean
   */
  Util.isNegativeNumber = function(obj){
    return typeof(obj) === 'number' && obj < 0;
  };

  Util.isIntegerNumber = function(obj){
    return typeof(obj) === 'number' && Number.isInteger(obj);
  };

  Util.isPositiveInteger = function(obj){
    return typeof(obj) === 'number' && Number.isInteger(obj) && obj >= 0;
  };

  Util.isNegativeInteger = function(obj){
    return typeof(obj) === 'number' && Number.isInteger(obj) && obj < 0;
  };

  return Util;

})();

