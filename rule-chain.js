module.exports = (function(){
  
  const util = require(`${__dirname}/validate-util.js`);
  
  const CommonRule = require(`${__dirname}/rules/common-rule.js`);

  const RequiredRule = require(`${__dirname}/rules/required-rule.js`);
 
  const TypeofRule = require(`${__dirname}/rules/typeof-rule.js`);

  const LengthRule = require(`${__dirname}/rules/length-rule.js`);

  const ValueRule = require(`${__dirname}/rules/value-rule.js`);

  const MatchRule = require(`${__dirname}/rules/match-rule.js`);

  const EmailRule = require(`${__dirname}/rules/email-rule.js`);

  const LogicRule = require(`${__dirname}/rules/logic-rule.js`);

  const ChildRule = require(`${__dirname}/rules/child-rule.js`);

  const ChildrenRule = require(`${__dirname}/rules/children-rule.js`);

  /**
   * RuleChain class
   */
  var Cls = function(){
    this.rules = [];
  };

  /**
   * @param options
   * @return RuleChain object
   */
  Cls.parse = function(options){
    if(! util.isNotNullObject(options)) throw new Error('Invalid validation options: ' + JSON.stringify(options, null, 4));

    var ins = new Cls();

    for(var name in options){
      var rule = null;

      switch(name){
        case 'required':
          rule = RequiredRule.parse(options[name]);
        break;
        case 'typeof':
          rule = TypeofRule.parse(options[name]);
        break;
        case 'length':
          rule = LengthRule.parse(options[name]);
        break;
        case 'value':
          rule = ValueRule.parse(options[name]);
        break;
        case 'match':
          rule = MatchRule.parse(options[name]);
        break;
        case 'email':
          rule = EmailRule.parse(options[name]);
        break;
        case 'logic':
          rule = LogicRule.parse(options[name]);
        break;
        case 'child':
          rule = ChildRule.parse(options[name]);
        break;
        case 'children':
          rule = ChildrenRule.parse(options[name]);
        break;
      }

      if(! rule) throw new Error('Invalid validation rule: ' + name);

      ins.addRule(rule);
    }

    if(! util.isNotEmptyArray(ins.rules)) throw new Error('Invalid validation options: ' + JSON.stringify(options, null, 4));

    return ins;
  };

  /**
   * @param id
   * @oaram obj
   * @param message
   */
  Cls.prototype.addError = function(id, obj, message){
    if(! this.errors){
      this.errors = [{
        id     : id,
        value  : obj,
        message: message
      }];
    }else{
      this.errors[this.errors.length] = {
        id     : id,
        value  : obj,
        message: message
      };
    }
  };

  /**
   *
   */
  Cls.prototype.clearErrors = function(){
    this.errors = [];
  };

  /**
   *
   */
  Cls.prototype.getErrors = function(){
    return this.errors ? this.errors : [];
  };

  /**
   * @param rule
   */
  Cls.prototype.addRule = function(rule){
    if(rule && (rule instanceof CommonRule)){
      if(! this.rules){
        this.rules = [rule];
      }else{
        this.rules[this.rules.length] = rule;
      }
    }
  };

  /**
   * @param obj
   * @param id
   */
  Cls.prototype.validate = function(obj, id){
    this.clearErrors();

    if(! this.rules){
      return true;
    }

    for(var rule of this.rules){
      if(! rule.validate(obj, id, (id, obj, message)=>{
          this.addError(id, obj, message);
        }, null, this)){

        return false;
      }
    }

    return true;
  };

  return Cls;

})();

