module.exports = function(){
  const UNDEFINED    = undefined;
  const NULL         = null;
  const EMPTY_ARRAY  = [];
  const EMPTY_STRING = '';
  const INT_ZERO     = 0;
  const FLOAT_ZERO   = 0.0;
  const FUNCTION     = ()=>{};
  const EMPTY_OBJECT = {};

  var V = require('./node-obj-validator.js');
  
  const assert_required_validate_true = (a, v)=>{
    a.assertFalse(v.validate(UNDEFINED  ), 'Validate UNDEFINED'   )
    .assertFalse(v.validate(NULL        ), 'Validate NULL'        )
    .assertFalse(v.validate(EMPTY_STRING), 'Validate EMPTY STRING')
    .assertFalse(v.validate(EMPTY_ARRAY ), 'Validate EMPTY_ARRAY' )

    .assertTrue(v.validate(INT_ZERO     ), 'Validate INTEGER ZERO')
    .assertTrue(v.validate(FLOAT_ZERO   ), 'Validate FLOAT ZERO'  )
    .assertTrue(v.validate(EMPTY_OBJECT ), 'Validate EMPTY OBJECT')
    .assertTrue(v.validate(FUNCTION     ), 'Validate FUNCTION'    )

    .assertTrue(v.validate('some string'), 'Validate some string' )
    .assertTrue(v.validate(true         ), 'Validate TRUE'        )
    .assertTrue(v.validate(false        ), 'Validate FALSE'       );
  };

  const assert_required_validate_false = (a, v)=>{
    a.assertTrue(v.validate(UNDEFINED   ), 'Validate UNDEFINED'   )
    .assertTrue(v.validate(NULL         ), 'Validate NULL'        )
    .assertTrue(v.validate(EMPTY_STRING ), 'Validate EMPTY STRING')
    .assertTrue(v.validate(EMPTY_ARRAY  ), 'Validate EMPTY_ARRAY' )

    .assertTrue(v.validate(INT_ZERO     ), 'Validate INTEGER ZERO')
    .assertTrue(v.validate(FLOAT_ZERO   ), 'Validate FLOAT ZERO'  )
    .assertTrue(v.validate(EMPTY_OBJECT ), 'Validate EMPTY OBJECT')
    .assertTrue(v.validate(FUNCTION     ), 'Validate FUNCTION'    )

    .assertTrue(v.validate('some string'), 'Validate some string' )
    .assertTrue(v.validate(true         ), 'Validate TRUE'        )
    .assertTrue(v.validate(false        ), 'Validate FALSE'       );
  };

  const assert_onok_msgs_one = (a, msgs, expectedId, expectedValue, expectedMessage)=>{
    a.assertClassName('Array', msgs, 'msgs is not Array')
    .assertEquals(1, msgs.length)
    .assertEquals(expectedId, msgs[0].id)
    .assertEquals(expectedValue, msgs[0].value)
    .assertEquals(expectedMessage, msgs[0].message);
  };

  return {
    // Test required rule : validate
    test_required_N000: (a)=>{
      a.assertNotError(()=>{
        var v = new V({
          required: true
        });
        assert_required_validate_true(a, v);
      });
    },
    test_required_N001: (a)=>{
      a.assertNotError(()=>{
        var v = new V({
          required: false
        });
        assert_required_validate_false(a, v);
      });
    },
    test_required_N002: (a)=>{
      a.assertNotError(()=>{
        var v = new V({
          required: 'some message'
        });
        assert_required_validate_true(a, v);
      });
    },
    test_required_N003: (a)=>{
      a.assertNotError(()=>{
        var v = new V({
          required: {
            message: 'some message'
          }
        });
        assert_required_validate_true(a, v);
      });
    },
    // Test required rule : onOk, onError
    test_required_N010: (a)=>{
      a.assertNotError(()=>{
        var v = new V({
          required: true
        });

        a.assertFalse(v.validate(UNDEFINED, (msgs)=>{
          assert_onok_msgs_one(a, msgs, UNDEFINED, UNDEFINED, 'Validation failed: parameter is required');
        }, ()=>{
          a.fail('onOk must not be executed');
        }));

        var rs = false;
        a.assertTrue(v.validate('some', (msgs)=>{
          a.fail('onError must not be executed');
        }, ()=>{
          rs = true;
        }));
        a.assertTrue(rs);
      });
    },
    test_required_N011: (a)=>{
      a.assertNotError(()=>{
        var v = new V({
          required: false
        });

        var rs = false;
        a.assertTrue(v.validate(UNDEFINED, (msgs)=>{
          a.fail('onError must not be executed');
        }, ()=>{
          rs = true;
        }));
        a.assertTrue(rs);
      });
    },
    test_required_N012: (a)=>{
      a.assertNotError(()=>{
        var v = new V({
          required: 'This is message'
        });

        a.assertFalse(v.validate(NULL, (msgs)=>{
          assert_onok_msgs_one(a, msgs, UNDEFINED, NULL, 'This is message');
        }, ()=>{
          a.fail('onOk must not be executed');
        }));

        var rs = false;
        a.assertTrue(v.validate('some', (msgs)=>{
          a.fail('onError must not be executed');
        }, ()=>{
          rs = true;
        }));
        a.assertTrue(rs);
      });
    },
    test_required_N013: (a)=>{
      a.assertNotError(()=>{
        var v = new V({
          required: {
            message: 'This is message'
          }
        });

        a.assertFalse(v.validate(EMPTY_STRING, (msgs)=>{
          assert_onok_msgs_one(a, msgs, UNDEFINED, EMPTY_STRING, 'This is message');
        }, ()=>{
          a.fail('onOk must not be executed');
        }));

        var rs = false;
        a.assertTrue(v.validate('some', (msgs)=>{
          a.fail('onError must not be executed');
        }, ()=>{
          rs = true;
        }));
        a.assertTrue(rs);
      });
    },
    // Test required rule : on_error, on_ok
    test_required_N020: (a)=>{
      a.assertNotError(()=>{
        var v = new V({
          required: {
            message: 'This is message',
            on_error: (id, obj, message)=>{
              a.assertUndefined(id);
              a.assertNull(obj);
              a.assertEquals('This is message', message);
            },
            on_ok: (id, obj)=>{
              a.fail('ok_ok must not be executed');
            }
          }
        });

        v.validate(NULL);
      });
    },
    test_required_N021: (a)=>{
      a.assertNotError(()=>{
        var v = new V({
          required: {
            message: 'This is message',
            on_error: (id, obj, message)=>{
              a.fail('on_error must not be executed');
            },
            on_ok: (id, obj)=>{
              a.assertUndefined(id);
              a.assertEquals('abc', obj);
            }
          }
        });

        v.validate('abc');
      });
    },
    // Test required rule : Error
    data_required_E000: ()=>{
      return [
        UNDEFINED
      , NULL
      , EMPTY_ARRAY
      , EMPTY_STRING
      , INT_ZERO
      , FLOAT_ZERO
      , FUNCTION
      , EMPTY_OBJECT
      , { something: 'something' }];
    },
    test_required_E000: (a, data)=>{
      a.assertError(()=>{
        var v = new V({ required: data });
      });
    },

  };
};

