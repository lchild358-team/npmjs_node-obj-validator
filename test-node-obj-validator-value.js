module.exports = function(){
  const UNDEFINED    = undefined;
  const NULL         = null;
  const EMPTY_ARRAY  = [];
  const EMPTY_STRING = '';
  const INT_ZERO     = 0;
  const FLOAT_ZERO   = 0.0;
  const FUNCTION     = ()=>{};
  const EMPTY_OBJECT = {};

  const V = require('./node-obj-validator.js');

  return {
    data_constructor_N000: ()=>{
      return [
        '',
        'abc',
        0,
        0.1,
        1,
        1.1,
        [undefined],
        [null],
        [''],
        ['abc'],
        [0],
        [0.1],
        [1],
        [1.1],
        [true],
        [false],
        [[]],
        [{}],
        [()=>{}]
      ];
    },
    test_constructor_N000: (assert, data)=>{
      assert.assertNotError(()=>{
        const v = new V({
          value: data
        });
      });
    },
    data_constructor_min_N000: ()=>{
      return [
        '',
        'abc',
        0,
        0.1,
        1,
        1.1
      ];
    },
    test_constructor_min_N000: (assert, data)=>{
      assert.assertNotError(()=>{
        const v = new V({
          value: {
            min: data
          }
        });
      });
    },
    data_constructor_max_N000: ()=>{
      return [
        '',
        'abc',
        0,
        0.1,
        1,
        1.1
      ];
    },
    test_constructor_max_N000: (assert, data)=>{
      assert.assertNotError(()=>{
        const v = new V({
          value: {
            max: data
          }
        });
      });
    },
    data_constructor_value_N000: ()=>{
      return [
        '',
        'abc',
        0,
        0.1,
        1,
        1.1,
        [undefined],
        [null],
        [''],
        ['abc'],
        [0],
        [0.1],
        [1],
        [1.1],
        [true],
        [false],
        [[]],
        [{}],
        [()=>{}]
      ];
    },
    test_constructor_value_N000: (assert, data)=>{
      assert.assertNotError(()=>{
        const v = new V({
          value: {
            value: data
          }
        });
      });
    },
    data_constructor_E000: ()=>{
      return [
        undefined,
        null,
        true,
        false,
        [],
        {},
        ()=>{}
      ];
    },
    test_constructor_E000: (assert, data)=>{
      assert.assertError(()=>{
        const v = new V({
          value: data
        });
      });
    },
    data_constructor_min_E000: ()=>{
      return [
        undefined,
        null,
        true,
        false,
        [],
        {},
        ()=>{}
      ];
    },
    test_constructor_min_E000: (assert, data)=>{
      assert.assertError(()=>{
        const v = new V({
          value: {
            min: data
          }
        });
      });
    },
    data_constructor_max_E000: ()=>{
      return [
        undefined,
        null,
        true,
        false,
        [],
        {},
        ()=>{}
      ];
    },
    test_constructor_max_E000: (assert, data)=>{
      assert.assertError(()=>{
        const v = new V({
          value: {
            max: data
          }
        });
      });
    },
    data_constructor_value_E000: ()=>{
      return [
        undefined,
        null,
        true,
        false,
        [],
        {},
        ()=>{}
      ];
    },
    test_constructor_value_E000: (assert, data)=>{
      assert.assertError(()=>{
        const v = new V({
          value: {
            value: data
          }
        });
      });
    },
  }
};
