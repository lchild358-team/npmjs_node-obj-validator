module.exports = exports = (function(){

  const RuleChain = require(`${__dirname}/rule-chain.js`);
  
  /**
   * Class NodeObjValidator
   * Constructor
   * @param rules object Validation rules
   */
  var Cls = function(options){
    this.rulechain = RuleChain.parse(options);
  };

  /**
   * Do validate the given object
   * @param obj mixed Destination object to be validated
   * @param options object (optional) Options of this validation
   * @param onError
   * @param onOk
   * @return boolean
   */
  Cls.prototype.validate = function(obj, onError, onOk, context){
    if(! this.rulechain.validate(obj)){
      if(typeof onError === 'function'){
        onError.call(context, this.rulechain.getErrors());
      }

      return false;
    }else{
      if(typeof onOk === 'function'){
        onOk.call(context);
      }

      return true;
    }
  };

  // Utilities functions
  Cls.Util = require(`${__dirname}/validate-util.js`);

  // Return class NodeObjValidatorn
  return Cls;

})();

