module.exports = function(){
  const UNDEFINED    = undefined;
  const NULL         = null;
  const EMPTY_ARRAY  = [];
  const EMPTY_STRING = '';
  const INT_ZERO     = 0;
  const FLOAT_ZERO   = 0.0;
  const FUNCTION     = ()=>{};
  const EMPTY_OBJECT = {};

  var V = require('./node-obj-validator.js');
  var util = V.Util;

  return {
    // Test static methods
    test_is_empty_string: (a)=>{
      for(var val of [undefined, null, 'a', ' ', 0, 0.0, [], {}, ()=>{}]){
        a.assertFalse(util.isEmptyString(val));
      }
      a.assertTrue(util.isEmptyString(''));
    },
    test_is_not_empty_string: (a)=>{
      for(var val of [undefined, null, '', 0, 0.0, [], {}, ()=>{}]){
        a.assertFalse(util.isNotEmptyString(val));
      }
      for(var val of ['a', ' ']){
        a.assertTrue(util.isNotEmptyString(val));
      }
    },
    // Test constructor
    data_constructor_E000: ()=>{
      return [UNDEFINED, NULL, EMPTY_ARRAY, EMPTY_STRING, INT_ZERO, FLOAT_ZERO, FUNCTION, EMPTY_OBJECT, {unknown: true}];
    },
    test_constructor_E000: (a, data)=>{
      a.assertError(()=>{
        var v = new V(data);
      });
    },
  }
};
