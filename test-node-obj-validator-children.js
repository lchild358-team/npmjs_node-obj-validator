module.exports = function(){

  const V = require('./node-obj-validator.js');

  return {
    data_constructor_N000: function(){
      return [
        {required: true}
      ];
    },
    test_constructor_N000: function(assert, data){
      assert.assertNotError(()=>{
        let v = new V({
          children: data
        });
      });
    },
    data_constructor_E000: function(){
      return [
        {},
        undefined,
        null,
        '',
        'a',
        true,
        false,
        0,
        1,
        1.2,
        [],
        function(){}
      ];
    },
    test_constructor_E000: function(assert, data){
      assert.assertError(()=>{
        let v = new V({
          children: data
        });
      });
    },
    data_validate_N000: function(){
      return [
        {},
        [],
        {a:1},
        {a:1,b:false,c:true},
        [0,1,'a']
      ];
    },
    test_validate_N000: function(assert, data){
      let v = new V({
        children: {
          required: true
        }
      });

      assert.assertTrue(v.validate(data));
    },
    data_validate_N001: function(){
      return [
        {a:undefined},
        {b:null},
        {c:1,d:undefined},
        [undefined],
        [1,null],
        [1,''],
        [1,2,[]]
      ];
    },
    test_validate_N001: function(assert, data){
      let v = new V({
        children: {
          required: true
        }
      });

      assert.assertFalse(v.validate(data));
    },
    test_onError_N000: function(assert, data){
      let v = new V({
        children: {
          required: true
        }
      });

      let value = {a:undefined, b:null, c:1};
      let errflg = false;
      assert.assertFalse(v.validate(value, (msgs)=>{
        assert.assertTypeof('object', msgs);
        assert.assertClassName('Array', msgs);
        assert.assertInstanceof(Array, msgs);
        assert.assertEquals(2, msgs.length);

        assert.assertEquals('a', msgs[0].id);
        assert.assertUndefined(msgs[0].value);
        assert.assertEquals('Validation failed: parameter is required', msgs[0].message);

        assert.assertEquals('b', msgs[1].id);
        assert.assertNull(msgs[1].value);
        assert.assertEquals('Validation failed: parameter is required', msgs[1].message);

        errflg = true;
      }, ()=>{
        assert.fail('onOk should not be called');
      }));
      assert.assertTrue(errflg, 'onError not be called');
    },
    test_onOk_N000: function(assert, data){
      let v = new V({
        children: {
          required: true
        }
      });

      let value = {a:'abc', b:{}, c:1, d:false, e:0};
      let rsflg = false;
      assert.assertTrue(v.validate(value, (msgs)=>{
        assert.fail('onError should not be called');
      }, ()=>{
        rsflg = true;
      }));
      assert.assertTrue(rsflg, 'onOk not be called');
    }
  }
};

